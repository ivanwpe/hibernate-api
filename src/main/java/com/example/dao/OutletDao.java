package com.example.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.model.Outlet;

@Repository
public interface OutletDao extends JpaRepository<Outlet, Long>{

}
