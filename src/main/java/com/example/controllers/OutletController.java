package com.example.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.dao.OutletDao;
import com.example.exception.ResourceNotFindException;
import com.example.model.Employee;
import com.example.model.Outlet;

@RestController
@RequestMapping("/api")
public class OutletController {
	
	@Autowired
	private OutletDao outletDao;
	
	//get outlet
	@GetMapping("/outlet")
	public List<Outlet> getAllOutlet() {
		return this.outletDao.findAll();
	}
	
	//get outlet by id
	@GetMapping("/outlet/{id}")
	public ResponseEntity<Outlet> getOutletById(@PathVariable(value = "id") Long outletId)
	throws ResourceNotFindException {
		Outlet outlet = outletDao.findById(outletId)
				.orElseThrow(() -> new ResourceNotFindException("Outlet not find for this id " + outletId));
		return ResponseEntity.ok().body(outlet);
	}
	
	//add outlet
	@PostMapping("/outlet")
	public String createOutlet(@RequestBody Outlet outlet) {
		this.outletDao.save(outlet);
		return "Success add outlet";
	}
	
	//update outlet
	@PutMapping("/outlet/{id}")
	public ResponseEntity<Outlet> updateOutlet(@PathVariable(value = "id") Long outletId,
			@Valid @RequestBody Outlet outletDetails) 
	throws ResourceNotFindException {
		Outlet outlet = outletDao.findById(outletId)
				.orElseThrow(() -> new ResourceNotFindException("Outlet not find for this id " + outletId));
		
		outlet.setOutletName(outletDetails.getOutletName());
		outlet.setOutletAddress(outletDetails.getOutletAddress());
		outlet.setOutletPhone(outletDetails.getOutletPhone());
		
		return ResponseEntity.ok(this.outletDao.save(outlet));		
	}
	
	//delete outlet
	@DeleteMapping("/outlet/{id}")
	public String deleteOutlet(@PathVariable(value = "id") Long outletId)
	throws ResourceNotFindException {
		Outlet outlet = outletDao.findById(outletId)
				.orElseThrow(() -> new ResourceNotFindException("Outlet not find for this id " + outletId));
		
		this.outletDao.delete(outlet);
		
		return "Deleted";
	}

}

