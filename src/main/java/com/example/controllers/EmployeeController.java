package com.example.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.dao.EmployeeDao;
import com.example.exception.ResourceNotFindException;
import com.example.model.Employee;

@RestController
@RequestMapping("/api")
public class EmployeeController {

	@Autowired
	private EmployeeDao employeeDao;
	
	//get employee
	@GetMapping("/employee")
	public List<Employee> getAllEmployees() {
		return this.employeeDao.findAll();
	}
	
	//get employee by Id
	@GetMapping("/employee/{id}")
	public ResponseEntity<Employee> getEmployeeById(@PathVariable(value = "id") Long employeeId)
	throws ResourceNotFindException {
		Employee employee = employeeDao.findById(employeeId)
				.orElseThrow(() -> new ResourceNotFindException("Employee not find for this id " + employeeId));
		return ResponseEntity.ok().body(employee);
	}
	
	//add employee
	@PostMapping("/employee")
	public String createEmployee(@RequestBody Employee employee) {
		this.employeeDao.save(employee);
		return "Success add employee";
	}
	
	//update employee
	@PutMapping("/employee/{id}")
	public ResponseEntity<Employee> updateEmployee(@PathVariable(value = "id") Long employeeId,
			@Valid @RequestBody Employee employeeDetails) 
	throws ResourceNotFindException {
		Employee employee = employeeDao.findById(employeeId)
				.orElseThrow(() -> new ResourceNotFindException("Employee not find for this id " + employeeId));
		
		employee.setName(employeeDetails.getName());
		employee.setAddress(employeeDetails.getAddress());
		employee.setPhone(employeeDetails.getPhone());
		employee.setSalary(employeeDetails.getSalary());
		
		return ResponseEntity.ok(this.employeeDao.save(employee));
		
	}
	
	//delete employee
	@DeleteMapping("/employee/{id}")
	public String deleteEmployee(@PathVariable(value = "id") Long employeeId)
	throws ResourceNotFindException {
		Employee employee = employeeDao.findById(employeeId)
				.orElseThrow(() -> new ResourceNotFindException("Employee not find for this id " + employeeId));
		
		this.employeeDao.delete(employee);
		
		return "Deleted";
	}
	
}
