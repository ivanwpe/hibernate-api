package com.example.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "outlet")
@org.hibernate.annotations.Entity(dynamicInsert = true)
public class Outlet implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long outletId;

	@ManyToOne
	@JoinColumn(name = "FK_EmployeeId")
	private Employee employeeId;

	@NotNull
	@Column(name = "OutletName", nullable = false)
	private String outletName;

	@NotNull
	@Column(name = "OutletAddress", nullable = false)
	private String outletAddress;

	@NotNull
	@Column(name = "OutletPhone", nullable = false)
	private String outletPhone;

	@Column(name = "CreatedAt", columnDefinition = "TIMESTAMP default NOW()")
	private Date createdAt;

	@Column(name = "CreatedBy", columnDefinition = "varchar(255) default 'SYSTEM'")
	private String createdBy;

	@Column(name = "UpdatedAt", columnDefinition = "TIMESTAMP default NOW()")
	private Date updatedAt;

	@Column(name = "UpdatedBy", columnDefinition = "varchar(255) default 'SYSTEM'")
	private String updateBy;

	public Outlet() {
		super();
	}

	public Outlet(@NotNull String outletName, @NotNull String outletAddress, @NotNull String outletPhone,
			Date createdAt, String createdBy, Date updatedAt, String updateBy) {
		super();
		this.outletName = outletName;
		this.outletAddress = outletAddress;
		this.outletPhone = outletPhone;
		this.createdAt = createdAt;
		this.createdBy = createdBy;
		this.updatedAt = updatedAt;
		this.updateBy = updateBy;
	}

	public Employee getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Employee employeeId) {
		this.employeeId = employeeId;
	}

	public long getOutletId() {
		return outletId;
	}

	public void setOutletId(long outletId) {
		this.outletId = outletId;
	}

	public String getOutletName() {
		return outletName;
	}

	public void setOutletName(String outletName) {
		this.outletName = outletName;
	}

	public String getOutletAddress() {
		return outletAddress;
	}

	public void setOutletAddress(String outletAddress) {
		this.outletAddress = outletAddress;
	}

	public String getOutletPhone() {
		return outletPhone;
	}

	public void setOutletPhone(String outletPhone) {
		this.outletPhone = outletPhone;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

}
