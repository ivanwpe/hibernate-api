package com.example.model;

import java.io.Serializable;
import java.util.*;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.*;

@Entity
@Table(name = "employee")
@org.hibernate.annotations.Entity(dynamicInsert = true)
public class Employee implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@OneToMany(mappedBy = "employeeId", cascade = CascadeType.ALL)
	Set<Outlet> outlet = new HashSet<Outlet>();

	@NotNull
	@Column(name = "Name", nullable = false)
	private String name;

	@NotNull
	@Column(name = "Address", nullable = false)
	private String address;

	@NotNull
	@Column(name = "Phone", nullable = false)
	private String phone;

	@Column(name = "EmergencyCall", nullable = true)
	private String emergencyCall;

	@NotNull
	@Column(name = "Salary", nullable = false)
	private String salary;

	@Column(name = "WorkedAt", columnDefinition = "TIMESTAMP default NOW()")
	private Date workedAt;

	@Column(name = "Resign")
	private Date resign;

	@Column(name = "CreatedAt", columnDefinition = "TIMESTAMP default NOW()")
	private Date createdAt;

	@Column(name = "CreatedBy", columnDefinition = "varchar(255) default 'SYSTEM'")
	private String createdBy;

	@Column(name = "UpdatedAt", columnDefinition = "TIMESTAMP default NOW()")
	private Date updatedAt;

	@Column(name = "UpdatedBy", columnDefinition = "varchar(255) default 'SYSTEM'")
	private String updateBy;

	public Employee() {
		super();
	}

	public Employee(String name, String address, String phone, String emergencyCall, String salary, Date workedAt,
			Date resign, Date createdAt, String createdBy, Date updatedAt, String updateBy) {
		super();
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.emergencyCall = emergencyCall;
		this.salary = salary;
		this.workedAt = workedAt;
		this.resign = resign;
		this.createdAt = createdAt;
		this.createdBy = createdBy;
		this.updatedAt = updatedAt;
		this.updateBy = updateBy;
	}

	public Set<Outlet> getOutlet() {
		return outlet;
	}

	public void setOutlet(Set<Outlet> outlet) {
		this.outlet = outlet;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmergencyCall() {
		return emergencyCall;
	}

	public void setEmergencyCall(String emergencyCall) {
		this.emergencyCall = emergencyCall;
	}

	public String getSalary() {
		return salary;
	}

	public void setSalary(String salary) {
		this.salary = salary;
	}

	public Date getWorkedAt() {
		return workedAt;
	}

	public void setWorkedAt(Date workedAt) {
		this.workedAt = workedAt;
	}

	public Date getResign() {
		return resign;
	}

	public void setResign(Date resign) {
		this.resign = resign;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

}
